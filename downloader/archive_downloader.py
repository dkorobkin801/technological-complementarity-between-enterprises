import asyncio
import aiofiles
from aiohttp import ClientSession
import aiohttp
from time import time as current_time
import os.path
import os
import requests
from tqdm import tqdm

from bs4 import BeautifulSoup as BS

def main():
    with open("urls.txt", "r") as f:
        urls = f.readlines()
    urls = list(map(lambda x: x.replace('\n', ""), urls))
    if os.path.isfile("downloaded.txt"):
        with open("downloaded.txt", "r") as f:
            downloaded = set(f.readlines())
    else:
        downloaded = set()

    urls = [url for url in urls if url not in downloaded]

    with open("downloaded.txt", "a") as downloaded_list_file:
        with open("broken.txt", "a") as broken_list_file:
            for url in urls:
                download(url, downloaded_list_file, broken_list_file)

def download(url, downloaded_list_file, broken_list_file):
    filename = url[url.rfind('/')+1:]
    if os.path.isfile(filename):
        print("File " + filename + " already exists")
        downloaded_list_file.write(url+'\n')
    else:
        print("Downloading " + url)
        response = requests.get(url, stream=True)
        total_size_in_bytes= int(response.headers.get('content-length', 0))
        block_size = 1024
        progress_bar = tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True)
        with open(filename+".tmp", 'wb') as file:
            for data in response.iter_content(block_size):
                progress_bar.update(len(data))
                file.write(data)
        progress_bar.close()
        if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
            print("Failed to download " + url)
            broken_list_file.write(url+'\n')
        else:
            print("Successfully downloaded " + url)
            downloaded_list_file.write(url+'\n')
            os.rename(filename+".tmp", filename)

if __name__ == "__main__":
    main()
