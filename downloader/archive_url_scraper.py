from bs4 import BeautifulSoup as BS
import asyncio
from aiohttp import ClientSession
from time import time as current_time
import re
from pprint import pprint

LAST_REQUEST_TIME = current_time()
DEFAULT_WAIT_TIME = 0.1

async def main():
    url_base = "https://bulkdata.uspto.gov/data/patent/grant/redbook/fulltext/"
    urls = [url_base + str(x) + '/' for x in range(2010,2021)]
    result = []
    with open("urls.txt", "a") as f:
        async with ClientSession() as session:
            for url in urls:
                html = await getPage(url, session)
                temp_res = scrapPage(html)
                temp_res = list(map(lambda x: url + x, temp_res))
                f.write('\n'.join(temp_res))
                print(f"Processed {url} with {len(temp_res)} files found")

def scrapPage(html):
    res = html.findAll("a", string=re.compile(r"ipg.*?\.zip"))
    res = list(map(lambda x: x['href'], res))
    return res

# Запрашивает страницу по адресу
async def getPage(url, session):
    html_text = await safeGet(url, session)
    html = BS(html_text, 'html.parser')
    return html

# Делает запрос с учётом времени между запросами
async def safeGet(url, session, wait_time=DEFAULT_WAIT_TIME):
    global LAST_REQUEST_TIME

    sleep_time = 0
    if abs(current_time() - LAST_REQUEST_TIME) < wait_time:
        sleep_time = abs(current_time() - LAST_REQUEST_TIME)

    LAST_REQUEST_TIME = current_time() + sleep_time
    if sleep_time > 0:
        await asyncio.sleep(sleep_time)

    resp = await session.request(method="GET", url=url)
    resp.raise_for_status()
    html = await resp.text()
    return html

if __name__ == "__main__":
    asyncio.run(main())
